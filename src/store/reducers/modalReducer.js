const initialState = {
  modalProduct: {},
  isOpen: false,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case "OPEN_MODAL":
      return { ...action.payload };

    case "SUBMIT_MODAL":
      return {
        ...state,
        isOpen: false,
        modalData: action.payload,
      };
    case "CLOSE_MODAL":
      return { ...action.payload };

    default:
      return state;
  }
};

//   const [showDeleteModal, setShowDeleteModal] = useState();
//   const onOpenDeleteModal = (product) => {
//     setShowDeleteModal(product);
//   };

//   const onCloseConfirmDeleteModal = () => {
//     setShowDeleteModal(null);
//   };
//   const onDeleteProduct = () => {
//     dispatch(removeCart(showDeleteModal));
//     setShowDeleteModal(null);
//   };
