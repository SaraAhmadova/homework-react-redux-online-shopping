// Install the redux, react-redux, and redux-thunk libraries.
// After receiving the data array through an AJAX request, store them in the redux store.
// When rendering components on the page, retrieve the data from the redux store.
// When opening any modal windows, the information about whether a modal window is currently open or not should be stored in the redux store.
// All actions should be implemented as functions using the redux-thunk middleware.

import { createStore, applyMiddleware } from 'redux';
import {thunk} from 'redux-thunk';
import {rootReducer} from './reducers';

const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
);

export default store;

