export const cartProducts = (product) => (dispatch, getState) => {
  const cartState = getState().cart;

  const existedProduct = cartState.findIndex(
    (cartElement) => cartElement.sku === product.sku
  );

  if (existedProduct !== -1) {
    const updatedCart = [...cartState];
    updatedCart[existedProduct]["count"] += 1;
    dispatch({
      type: "GET_CART_PRODUCTS",
      payload: updatedCart,
    });
  } else {
    dispatch({
      type: "GET_CART_PRODUCTS",
      payload: [...cartState, { ...product, count: 1 }],
    });
  }
};
export const addToCartProducts = (product) => (dispatch, getState) => {
  const cart = getState().cart;
  const existingCartIndex = cart.findIndex(
    (cartItem) => cartItem.sku === product.sku
  );
  const updatedCart = cart.map((cartElement) =>
    cartElement.sku === product.sku
      ? {
          ...cartElement,
          count: cartElement.count + 1,
        }
      : cartElement
  );
  if (existingCartIndex > -1) {
    dispatch({
      type: "GET_CART_PRODUCTS",
      payload: [...updatedCart],
    });
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  } else {
    dispatch({
      type: "GET_CART_PRODUCTS",
      payload: [...updatedCart, { ...product, count: 1 }],
    });
    localStorage.setItem(
      "cart",
      JSON.stringify([...updatedCart, { ...product, count: 1 }])
    );
  }
};

export const removeCart = (product) => (dispatch, getState) => {
  const cartState = getState().cart;
  const filteredCart = cartState.filter(
    (cartElement) => cartElement.sku !== product.sku
  );

  dispatch({
    type: "GET_CART_PRODUCTS",
    payload: filteredCart,
  });
  localStorage.setItem("cart", JSON.stringify(filteredCart));
};
