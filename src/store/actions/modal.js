export const openModalAction = (product) => (dispatch, getState) => {
  dispatch({
    type: "OPEN_MODAL",
    payload: { modalProduct: { ...product }, isOpen: true },
  });
};

export const closeModalAction = (dispatch, getState) => {
  dispatch({
    type: "CLOSE_MODAL",
    payload: { modalProduct: {}, isOpen: false },
  });
};
// export const submitModalAction = (dispatch, getState) => {
//   const modalData = getState().modalData;

//   dispatch({ type: "CLOSE_MODAL", payload: { ...modalData, isOpen: false } });
// };

//   const [showDeleteModal, setShowDeleteModal] = useState();
//   const onOpenDeleteModal = (product) => {
//     setShowDeleteModal(product);
//   };

//   const onCloseConfirmDeleteModal = () => {
//     setShowDeleteModal(null);
//   };
//   const onDeleteProduct = () => {
//     dispatch(removeCart(showDeleteModal));
//     setShowDeleteModal(null);
//   };
