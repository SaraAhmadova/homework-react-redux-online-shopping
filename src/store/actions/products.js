
export const fetchProducts = async (dispatch) => {
  fetch("./products.json")
    .then((response) => response.json())
    .then((data) => {
        console.log("fdsds",data);
      return dispatch({ type: "GET_PRODUCTS", payload: data });
    })
    .catch((error) => {
      console.error("Error fetching data:", error);
    });
};
