import React, {useEffect} from "react";
import "./App.css";
import Router from "./router";
import { useDispatch } from "react-redux";
import { fetchProducts } from "./store/actions/products";
const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts);
  }, [dispatch]);
  return <Router />;
};

export default App;
