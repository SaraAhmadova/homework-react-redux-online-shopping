import React from "react";
import ProductCard from "../../components/ProductCard";
import ConfirmDeleteModal from "../../components/ConfirmDeleteModal";
import { Header } from "../../components/header";
import { useSelector } from "react-redux";

const CartPage = () => {
  const productList = useSelector((state) => state.cart);
  const modalState = useSelector((state) => state.modal);


  return (
    <div>
      <h1>Cart page</h1>
      <Header />
      <div className="product-list">
        {productList?.length > 0 &&
          productList.map((product, index) => (
            <ProductCard
              key={index}
              product={product}
              isCartProduct={true}
            />
          ))}
      </div>
      {modalState.isOpen && <ConfirmDeleteModal />}
    </div>
  );
};

export default CartPage;
