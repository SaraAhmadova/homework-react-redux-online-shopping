import React from "react";
import PropTypes from "prop-types";
import "./ProductCard.css";
import { useDispatch } from "react-redux";
import { addToCartProducts } from "../../store/actions/cart";
import { favoriteProducts } from "../../store/actions/favorites";
import { openModalAction } from "../../store/actions/modal";

const ProductCard = ({
  product,
  isFavorite,
  isCartProduct,
}) => {
  const dispatch = useDispatch();
  const onAddToCart = (product) => {
    dispatch(addToCartProducts(product));
  };
  const onDeleteProduct = () => {
    dispatch(openModalAction(product));
  };
  const onToggleFavorite = (product) => {
    dispatch(favoriteProducts(product));
  };
  return (
    <div className={`product-card ${isCartProduct ? "cart-product-card" : ""}`}>
      <img
        src={product.imagePath}
        alt={product.name}
        className="product-image"
      />
      <h3 className="product-name">{product.name}</h3>
      <p className="product-price">${product.price}</p>
      <p className="product-color">Color: {product.color}</p>
      {isCartProduct && (
        <>
          <button onClick={onDeleteProduct} className="card-btn-delete">
            <span role="img" aria-label="remove product">
              &times;
            </span>
          </button>
          <button>{product.count}</button>
        </>
      )}
      {!isCartProduct && (
        <>
          <button onClick={() => onAddToCart(product)} className="card-btn">
            <span role="img" aria-label="cart">
              🛒
            </span>
          </button>
          <button
            onClick={() => onToggleFavorite(product)}
            className="card-btn"
          >
            {isFavorite ? (
              <span className="favorite-star">&#9733;</span>
            ) : (
              <span className="unfavorite-star">&#9733;</span>
            )}
          </button>
        </>
      )}
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imagePath: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    count: PropTypes.number,
  }).isRequired,
  isCartProduct: PropTypes.bool,
  isFavorite: PropTypes.bool,
};
export default ProductCard;
